#!/usr/bin/env python

import sys

from PyQt5.QtCore import QSize
from PyQt5.QtGui import QColor, QSurfaceFormat
from PyQt5.QtWidgets import (QApplication, QOpenGLWidget)

class GLWidget(QOpenGLWidget):

    def __init__(self, parent=None):
        super(GLWidget, self).__init__(parent)

        self.trolltechGreen = QColor.fromCmykF(0.40, 0.0, 1.0, 0.0)

    def minimumSizeHint(self):
        return QSize(640, 480)

    def sizeHint(self):
        return QSize(640, 480)

    def initializeGL(self):
        self.gl = self.context().versionFunctions()
        self.gl.initializeOpenGLFunctions()

        print 'RENDERER: %s' % self.gl.glGetString(self.gl.GL_RENDERER)
        print 'VERSION: %s' % self.gl.glGetString(self.gl.GL_VERSION)

        self.setClearColor(self.trolltechGreen.darker())
        self.gl.glEnable(self.gl.GL_DEPTH_TEST)
        self.gl.glDepthFunc(self.gl.GL_LESS)

    def paintGL(self):
        self.gl.glClear(
                self.gl.GL_COLOR_BUFFER_BIT | self.gl.GL_DEPTH_BUFFER_BIT)

    def setClearColor(self, c):
        self.gl.glClearColor(c.redF(), c.greenF(), c.blueF(), c.alphaF())

if __name__ == '__main__':

    app = QApplication(sys.argv)

    format = QSurfaceFormat()
    format.setDepthBufferSize(24)
    format.setStencilBufferSize(8)
    format.setVersion(3, 2)
    format.setProfile(QSurfaceFormat.CoreProfile)
    QSurfaceFormat.setDefaultFormat(format)

    glWidget = GLWidget()
    glWidget.setFormat(format)

    glWidget.show()
    sys.exit(app.exec_())
