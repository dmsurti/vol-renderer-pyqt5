#!/usr/bin/env python

import sys
import array

from PyQt5.QtCore import QEvent
from PyQt5.QtGui import (QGuiApplication, QMatrix4x4, QOpenGLContext, QColor,
                         QOpenGLShader, QOpenGLShaderProgram, QSurfaceFormat,
                         QOpenGLVertexArrayObject, QOpenGLBuffer ,QWindow)


class OpenGLWindow(QWindow):
    def __init__(self, parent=None):
        super(OpenGLWindow, self).__init__(parent)

        self.m_update_pending = False
        self.m_animating = False # disable vsync
        self.m_context = None
        self.m_gl = None

        self.setSurfaceType(QWindow.OpenGLSurface)

    def initialize(self):
        pass

    def setAnimating(self, animating):
        self.m_animating = animating

        if animating:
            self.renderLater()

    def renderLater(self):
        if not self.m_update_pending:
            self.m_update_pending = True
            QGuiApplication.postEvent(self, QEvent(QEvent.UpdateRequest))

    def renderNow(self):
        if not self.isExposed():
            return

        self.m_update_pending = False

        needsInitialize = False

        if self.m_context is None:

            self.m_context = QOpenGLContext(self)
            self.m_context.setFormat(self.requestedFormat())
            self.m_context.create()

            needsInitialize = True

        self.m_context.makeCurrent(self)

        if needsInitialize:
            self.m_gl = self.m_context.versionFunctions()
            self.m_gl.initializeOpenGLFunctions()

            print 'RENDERER: %s' % self.m_gl.glGetString(self.m_gl.GL_RENDERER)
            print 'VERSION: %s' % self.m_gl.glGetString(self.m_gl.GL_VERSION)

            self.initialize()

        self.render(self.m_gl)

        self.m_context.swapBuffers(self)

        if self.m_animating:
            self.renderLater()

    def event(self, event):
        if event.type() == QEvent.UpdateRequest:
            self.renderNow()
            return True

        return super(OpenGLWindow, self).event(event)

    def exposeEvent(self, event):
        self.renderNow()

    def resizeEvent(self, event):
        self.renderNow()


class TriangleWindow(OpenGLWindow):
    vertexShaderSource = '''
#version 410
in vec3 vp;
void main () {
        gl_Position = vec4 (vp, 1.0);
}
'''

    fragmentShaderSource = '''
#version 410
uniform vec4 input_color;
out vec4 frag_colour;
void main () {
	frag_colour = input_color;
}
'''

    def __init__(self):
        super(TriangleWindow, self).__init__()

        self.m_vao = 0
        self.m_vbo = 0
        self.m_idx = 0
        self.m_program = 0
        self.m_frame = 0

        self.m_colAttr = 0

    def initialize(self):
        self.m_vao = QOpenGLVertexArrayObject(self)
        self.m_vao.create()
        self.m_vao.bind()

        vertices = array.array('f', [
                 0.0,  0.5, 0.0,
                 0.5, -0.5, 0.0,
                -0.5, -0.5, 0.0])
        vertex_count = 3 # 3 vertices
        self.m_vbo = QOpenGLBuffer(QOpenGLBuffer.VertexBuffer)
        self.m_vbo.create()
        self.m_vbo.bind()
        self.m_vbo.setUsagePattern(QOpenGLBuffer.StaticDraw)
        self.m_vbo.allocate(vertices, vertex_count * 3 * 4) # 4 is sizeof float

        indices = array.array('i', [0, 1, 2])
        index_count = 3 # 3 indices
        self.m_idx = QOpenGLBuffer(QOpenGLBuffer.IndexBuffer)
        self.m_idx.create()
        self.m_idx.bind()
        self.m_idx.setUsagePattern(QOpenGLBuffer.StaticDraw)
        self.m_idx.allocate(indices, index_count * 4) # 4 is sizeof int

        self.m_program = QOpenGLShaderProgram(self)
        self.m_program.addShaderFromSourceCode(QOpenGLShader.Vertex,
                self.vertexShaderSource)
        self.m_program.addShaderFromSourceCode(QOpenGLShader.Fragment,
                self.fragmentShaderSource)
        self.m_program.enableAttributeArray(0)
        self.m_program.setAttributeBuffer(0, self.m_gl.GL_FLOAT, 0, 3)
        
        self.m_program.link()

        self.m_colAttr = self.m_program.uniformLocation('input_color')
        self.trolltechGreen = QColor.fromCmykF(0.40, 0.0, 1.0, 0.0)

    def render(self, gl):
        gl.glViewport(0, 0, self.width(), self.height())

        gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)

        self.m_program.bind()
        self.m_vao.bind()
        self.m_idx.bind()

        self.m_program.setUniformValue(self.m_colAttr, self.trolltechGreen)
        gl.glDrawElements(gl.GL_TRIANGLES, 3, gl.GL_UNSIGNED_INT, None)

        self.m_program.release()

        self.m_frame += 1


if __name__ == '__main__':

    app = QGuiApplication(sys.argv)

    format = QSurfaceFormat()
    format.setDepthBufferSize(24)
    format.setStencilBufferSize(8)
    format.setVersion(3, 2)
    format.setSamples(4) # MSAA is 4
    format.setProfile(QSurfaceFormat.CoreProfile)
    QSurfaceFormat.setDefaultFormat(format)

    window = TriangleWindow()
    window.setFormat(format) # this format is requested
    window.resize(640, 480)
    window.show()

    window.setAnimating(True) # use vysnc

    sys.exit(app.exec_())
