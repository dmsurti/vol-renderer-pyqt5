# Read the 3D images into an OpenGL 3D texture

import os
import numpy as np
from PIL import Image

class TextureLoader(object):
    def __init__(self, images_dir):
        self.images_dir = images_dir

        self.images = []
        self.texture = 0
        self.count = 0
        self.width = 0
        self.height = 0

        self.data = None

        self.initialize()

    def initialize(self):
        """
        Create 3D image data from the set of 2D images in dir
        """
        # list images in directory
        image_files = sorted(os.listdir(self.images_dir))
        print('loading images from: %s' % self.images_dir)
        for image_file in image_files:
            file_path = os.path.abspath(os.path.join(self.images_dir, image_file))
            try:
                # read image
                img = Image.open(file_path)
                img_data = np.array(img.getdata(), np.uint8)

                # check if all are of the same size
                if self.count is 0:
                    self.width, self.height = img.size[0], img.size[1]
                    self.images.append(img_data)
                else:
                    if (self.width, self.height) == (img.size[0], img.size[1]):
                        self.images.append(img_data)
                    else:
                        print('mismatch')
                        raise RunTimeError("image size mismatch")
                self.count += 1
            except:
                # skip
                print('Invalid image: %s' % file_path)

        # load image data into single array
        self.depth = self.count
        self.data = np.concatenate(self.images).tolist()
        print('volume data dims: %d %d %d' % (self.width, self.height, self.depth))

    def load_gl_3d_texture(self, gl):
        """
        Load 3D image data into opengl 3D texture
        """
        self.texture = gl.glGenTextures(1)
        gl.glPixelStorei(gl.GL_UNPACK_ALIGNMENT,1)
        gl.glBindTexture(gl.GL_TEXTURE_3D, self.texture)
        gl.glTexParameterf(gl.GL_TEXTURE_3D, gl.GL_TEXTURE_WRAP_S, gl.GL_CLAMP_TO_EDGE)
        gl.glTexParameterf(gl.GL_TEXTURE_3D, gl.GL_TEXTURE_WRAP_T, gl.GL_CLAMP_TO_EDGE)
        gl.glTexParameterf(gl.GL_TEXTURE_3D, gl.GL_TEXTURE_WRAP_R, gl.GL_CLAMP_TO_EDGE)
        gl.glTexParameterf(gl.GL_TEXTURE_3D, gl.GL_TEXTURE_MAG_FILTER, gl.GL_LINEAR)
        gl.glTexParameterf(gl.GL_TEXTURE_3D, gl.GL_TEXTURE_MIN_FILTER, gl.GL_LINEAR)
        gl.glTexImage3D(gl.GL_TEXTURE_3D, 0, gl.GL_RED,
                        self.width, self.height, self.depth, 0,
                        gl.GL_RED, gl.GL_UNSIGNED_BYTE, self.data)
        print 'Created 3D volume texture %s' % self.texture
