import array
from geometry import Geometry

# cube vertices
vertices = array.array('f', [
        0.0, 0.0, 0.0,
        1.0, 0.0, 0.0,
        1.0, 1.0, 0.0,
        0.0, 1.0, 0.0,
        0.0, 0.0, 1.0,
        1.0, 0.0, 1.0,
        1.0, 1.0, 1.0,
        0.0, 1.0, 1.0
        ])

# cube colors
colors = array.array('f', [
        0.0, 0.0, 0.0,
        1.0, 0.0, 0.0,
        1.0, 1.0, 0.0,
        0.0, 1.0, 0.0,
        0.0, 0.0, 1.0,
        1.0, 0.0, 1.0,
        1.0, 1.0, 1.0,
        0.0, 1.0, 1.0
        ])

# individual triangles
indices = array.array('i', [
        4, 5, 7,
        7, 5, 6,
        5, 1, 6,
        6, 1, 2,
        1, 0, 2,
        2, 0, 3,
        0, 4, 3,
        3, 4, 7,
        6, 2, 7,
        7, 2, 3,
        4, 0, 5,
        5, 0, 1
        ])

cube_vs = '''
#version 410

layout(location = 0) in vec3 cubePos;
layout(location = 1) in vec3 cubeCol;

uniform mat4 uMVMatrix;
uniform mat4 uPMatrix;

out vec4 vColor;

void main()
{
    // set position
    gl_Position = uPMatrix * uMVMatrix * vec4(cubePos.xyz, 1.0);

    // set color
    vColor = vec4(cubeCol.rgb, 1.0);
}
'''

front_cube_fs = '''
#version 410

in vec4 vColor;
out vec4 fragColor;

void main()
{
    fragColor = vColor;
}
'''

def make_front_cube():
    geometry = Geometry(vertices, colors, indices,
                        cube_vs, front_cube_fs, front=True)
    return geometry

back_cube_fs = '''
#version 410

in vec4 vColor;

uniform sampler2D texBackFaces;
uniform sampler3D texVolume;
uniform vec2 uWinDims;

out vec4 fragColor;

void main()
{
    // end of ray
    vec3 end = vColor.rgb;

    // calculate texture coords at fragment, which is a
    // fraction of window coords
    vec2 texc = gl_FragCoord.xy/uWinDims.xy;

    // get start of ray = front-face color
    vec3 start = texture(texBackFaces, texc).rgb;

    // calculate ray direction
    vec3 dir = end - start;

    // normalized ray direction
    vec3 norm_dir = normalize(dir);

    // the length from front to back is calculated and
    // used to terminate the ray
    float len = length(dir.xyz);

    // ray step size
    float stepSize = 0.01;

    // X-Ray projection
    vec4 dst = vec4(0.0);
    // step through the ray
    for(float t = 0.0; t < len; t += stepSize) {

        // set position to end point of ray
        vec3 samplePos = start + t*norm_dir;

        // get texture value at position
        float val = texture(texVolume, samplePos).r;
        vec4 src = vec4(val);

        // set opacity
        src.a *= 0.1;
        src.rgb *= src.a;

        // blend with previous value
        dst = (1.0 - dst.a)*src + dst;
        // exit loop when alpha exceeds threshold
        if(dst.a >= 0.95)
            break;
    }
    // set fragment color
    fragColor =  dst;
}
'''

def make_back_cube():
    geometry = Geometry(vertices, colors, indices,
                        cube_vs, back_cube_fs)
    return geometry
