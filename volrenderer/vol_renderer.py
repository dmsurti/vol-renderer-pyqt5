#!/usr/bin/env python

import sys, argparse
import array

from PyQt5.QtCore import QEvent
from PyQt5.QtGui import (QGuiApplication, QMatrix4x4, QOpenGLContext, QColor,
                         QOpenGLShader, QOpenGLShaderProgram, QSurfaceFormat,
                         QOpenGLVertexArrayObject, QOpenGLBuffer ,QWindow)

from texture_loader import TextureLoader
from scene import Scene

class OpenGLWindow(QWindow):
    def __init__(self, parent=None):
        super(OpenGLWindow, self).__init__(parent)

        self.m_update_pending = False
        self.m_animating = False # disable vsync
        self.m_context = None
        self.m_gl = None

        self.setSurfaceType(QWindow.OpenGLSurface)

    def initialize(self):
        pass

    def setAnimating(self, animating):
        self.m_animating = animating

        if animating:
            self.renderLater()

    def renderLater(self):
        if not self.m_update_pending:
            self.m_update_pending = True
            QGuiApplication.postEvent(self, QEvent(QEvent.UpdateRequest))

    def renderNow(self):
        if not self.isExposed():
            return

        self.m_update_pending = False

        needsInitialize = False

        if self.m_context is None:

            self.m_context = QOpenGLContext(self)
            self.m_context.setFormat(self.requestedFormat())
            self.m_context.create()

            needsInitialize = True

        self.m_context.makeCurrent(self)

        if needsInitialize:
            self.m_gl = self.m_context.versionFunctions()
            self.m_gl.initializeOpenGLFunctions()

            print 'RENDERER: %s' % self.m_gl.glGetString(self.m_gl.GL_RENDERER)
            print 'VERSION: %s' % self.m_gl.glGetString(self.m_gl.GL_VERSION)

            self.initialize()

        self.render(self.m_gl)

        self.m_context.swapBuffers(self)

        if self.m_animating:
            self.renderLater()

    def event(self, event):
        if event.type() == QEvent.UpdateRequest:
            self.renderNow()
            return True

        return super(OpenGLWindow, self).event(event)

    def exposeEvent(self, event):
        self.renderNow()

    def resizeEvent(self, event):
        self.renderNow()


class VolRendererWindow(OpenGLWindow):

    def __init__(self, scene):
        super(VolRendererWindow, self).__init__()

        self.scene = scene

        self.frame = 0

    def initialize(self):
        self.scene.initialize(self, self.m_gl)

    def render(self, gl):
        gl.glViewport(0, 0, self.width(), self.height())

        self.scene.render(gl)

        self.frame += 1



if __name__ == '__main__':

    # create parser
    parser = argparse.ArgumentParser(description="GPU acclerated Volume Rendering...")
    # add expected arguments
    parser.add_argument('--dir', dest='image_dir', required=True)
    # parse args
    args = parser.parse_args()

    app = QGuiApplication(sys.argv)

    format = QSurfaceFormat()
    format.setDepthBufferSize(24)
    format.setStencilBufferSize(8)
    format.setVersion(3, 2)
    format.setSamples(4) # MSAA is 4
    format.setProfile(QSurfaceFormat.CoreProfile)
    QSurfaceFormat.setDefaultFormat(format)

    texture_loader = TextureLoader(args.image_dir)
    scene = Scene(texture_loader)
    window = VolRendererWindow(scene)
    window.setFormat(format) # this format is requested
    window.resize(512, 512)
    window.show()

    window.setAnimating(True) # use vysnc

    sys.exit(app.exec_())
