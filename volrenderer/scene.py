from texture_loader import TextureLoader
from cube import make_back_cube, make_front_cube

class Scene(object):

    def __init__(self, texture_loader=None):
        self.texture_loader = texture_loader
        self.front_cube = make_front_cube()
        self.back_cube = make_back_cube()

    def initialize(self, parent, gl):
        self.texture_loader.load_gl_3d_texture(gl)
        self.front_cube.initialize(parent, gl)
        print 'AFTER: FBO Texture: %s' % self.front_cube.fbo.texture()
        self.back_cube.tex_volume = self.texture_loader.texture
        self.back_cube.fbo_tex = self.front_cube.fbo.texture()
        self.back_cube.initialize(parent, gl)

    def render(self, gl):
        self.front_cube.render(gl)
        self.back_cube.render(gl)
