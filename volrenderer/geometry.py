from PyQt5.QtCore import QEvent
from PyQt5.QtGui import (QGuiApplication, QMatrix4x4, QOpenGLContext, QColor,
                         QOpenGLShader, QOpenGLShaderProgram, QSurfaceFormat,
                         QOpenGLVertexArrayObject, QOpenGLBuffer ,QWindow,
                         QOpenGLFramebufferObject, QVector2D, QVector3D, QImage)

class Geometry(object):

    def __init__(self, vertices, colors, indices, vs_str, fs_str, front=False):
        self.vertices = vertices
        self.vertex_count = len(vertices) / 3
        self.colors = colors
        self.color_count = len(colors) / 3
        self.indices = indices
        self.index_count = len(indices)

        print self.vertex_count, self.color_count, self.index_count

        self.vertexShaderSource = vs_str
        self.fragmentShaderSource = fs_str

        self.vao = 0
        self.v_vbo = 0
        self.c_vbo = 0
        self.idx = 0
        self.program = 0

        self.tex_volume = 0

        self.front = front

        self.fbo = 0
        self.fbo_tex = 0
        self.width = 512
        self.height = 512

        self.center = QVector3D(0.5, 0.5, 0.5)
        self.eye = QVector3D(2.0, 0.5, 0.5)
        self.up = QVector3D(0.0, 0.0, 1.0)

        self.mv_mat = QMatrix4x4()
        self.mv_mat.lookAt(self.eye, self.center, self.up)

        self.p_mat = QMatrix4x4()
        self.p_mat.perspective(45.0, 1.0, 0.1, 100.0)

    def initialize(self, parent, gl):
        print 'Initialize geometry ...'

        self.vao = QOpenGLVertexArrayObject(parent)
        self.vao.create()
        self.vao.bind()

        self.v_vbo = QOpenGLBuffer(QOpenGLBuffer.VertexBuffer)
        self.v_vbo.create()
        self.v_vbo.bind()
        self.v_vbo.setUsagePattern(QOpenGLBuffer.StaticDraw)
        self.v_vbo.allocate(self.vertices,
                            self.vertex_count * 3 * 4)

        self.c_vbo = QOpenGLBuffer(QOpenGLBuffer.VertexBuffer)
        self.c_vbo.create()
        self.c_vbo.bind()
        self.c_vbo.setUsagePattern(QOpenGLBuffer.StaticDraw)
        self.c_vbo.allocate(self.colors,
                            self.color_count * 3 * 4)

        self.idx = QOpenGLBuffer(QOpenGLBuffer.IndexBuffer)
        self.idx.create()
        self.idx.bind()
        self.idx.setUsagePattern(QOpenGLBuffer.StaticDraw)
        self.idx.allocate(self.indices, self.index_count * 4)

        self.program = QOpenGLShaderProgram(parent)
        self.program.addShaderFromSourceCode(QOpenGLShader.Vertex,
              self.vertexShaderSource)
        self.program.addShaderFromSourceCode(QOpenGLShader.Fragment,
              self.fragmentShaderSource)

        # enable vertex buffer
        self.program.enableAttributeArray(0)
        self.program.setAttributeBuffer(0, gl.GL_FLOAT, 0, 3)

        # enable color buffer
        self.program.enableAttributeArray(1)
        self.program.setAttributeBuffer(1, gl.GL_FLOAT, 0, 3)

        self.program.link()

        if self.front:
            print 'Initialize FBO ...'
            self.fbo = QOpenGLFramebufferObject(self.width, self.height)
            self.fbo.setAttachment(QOpenGLFramebufferObject.Depth)
            print 'created fbo : %s' % self.fbo

    def render(self, gl):
        gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)

        self.program.bind()
        self.vao.bind()
        self.idx.bind()

        mv_mat = self.program.uniformLocation('uMVMatrix')
        p_mat = self.program.uniformLocation('uPMatrix')
        self.program.setUniformValue(mv_mat, self.mv_mat)
        self.program.setUniformValue(p_mat, self.p_mat)
 
        # enable face culling
        if self.front:  # implies front cube
            gl.glFrontFace(gl.GL_CCW)
            gl.glCullFace(gl.GL_BACK)
            gl.glEnable(gl.GL_CULL_FACE)
            self.fbo.bind()
        else: # implies back cube
            gl.glFrontFace(gl.GL_CCW)
            gl.glCullFace(gl.GL_FRONT)
            gl.glEnable(gl.GL_CULL_FACE)
            win_dims = self.program.uniformLocation('uWinDims')
            tex_back_faces = self.program.uniformLocation('texBackFaces')
            tex_volume = self.program.uniformLocation('texVolume')

            dims = QVector2D(float(self.width), float(self.height)) 
            self.program.setUniformValue(win_dims, dims)

            # texture unit 0 - back-faces of cube
            gl.glActiveTexture(gl.GL_TEXTURE0)
            gl.glBindTexture(gl.GL_TEXTURE_2D, self.fbo_tex)
            self.program.setUniformValue(tex_back_faces, 0)

            # texture unit 1 - 3D volume texture
            gl.glActiveTexture(gl.GL_TEXTURE1)
            gl.glBindTexture(gl.GL_TEXTURE_3D, self.tex_volume)
            self.program.setUniformValue(tex_volume, 1)


        # Now draw the geometry
        gl.glDrawElements(gl.GL_TRIANGLES, self.index_count,
                          gl.GL_UNSIGNED_INT, None)

        self.program.release()
        self.vao.release()
        self.idx.release()
        if self.front:
            gl.glDisable(gl.GL_CULL_FACE)
            self.fbo.release()
